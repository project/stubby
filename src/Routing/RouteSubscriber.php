<?php

namespace Drupal\stubby\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Provides dynamic Stub routes.
 *
 * @package Drupal\stubby\Routing
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * StubRoutes Service.
   *
   * @var \Drupal\stubby\Routing\StubRoutes
   */
  protected $stubRoutes;

  /**
   * RouteSubscriber constructor.
   *
   * @param \Drupal\stubby\Routing\StubRoutes $stub_routes
   *   StubRoutes Service.
   */
  public function __construct(StubRoutes $stub_routes) {
    $this->stubRoutes = $stub_routes;
  }

  /**
   * Adds Stubby RouteCollection to Drupal RouteCollection.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   *   Drupal RouteCollection.
   */
  protected function alterRoutes(RouteCollection $collection) {
    $routes = $this->stubRoutes->routes();
    $routes->addNamePrefix('stubby.');
    $collection->addCollection($routes);
  }

}
