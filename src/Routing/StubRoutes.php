<?php

namespace Drupal\stubby\Routing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Provides dynamic Stub routes.
 *
 * @package Drupal\stubby\Routing
 */
class StubRoutes {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * StubRoutes constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityManager = $entity_type_manager;
  }

  /**
   * Creates dynamic routes based on stubs.
   *
   * @return \Symfony\Component\Routing\RouteCollection
   *   Stubby RouteCollection
   */
  public function routes() {
    $routeCollection = new RouteCollection();

    $stubs = $this->getAllStubs();

    foreach ($stubs as $stub) {
      // Ignore stubs that have been disabled.
      if (!$stub->status()) {
        continue;
      }

      $routeCollection->add(
        $stub->id(),
        $this->createRouteFromStub($stub)
      );
    }

    return $routeCollection;
  }

  /**
   * Creates a Route from a Stub entity.
   *
   * @param \Drupal\stubby\Entity\StubInterface $stub
   *   Stub Entity.
   *
   * @return \Symfony\Component\Routing\Route
   *   Stub Route.
   */
  private function createRouteFromStub($stub) {
    return new Route(
      $stub->getPath(),
      [
        '_controller' => '\Drupal\stubby\Controller\Output::response',
        'stub' => $stub->id(),
      ],
      ['_access' => 'TRUE'],
      [],
      '',
      [],
      $this->getStubEnabledMethods($stub->getMethods())
    );
  }

  /**
   * Load all the Stubs from the Storage.
   *
   * @return \Drupal\stubby\Entity\StubInterface[]
   *   Array with Stub Entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getAllStubs() {
    return $this->entityManager->getStorage('stub')->loadMultiple();
  }

  /**
   * Gets enabled Stub methods.
   *
   * @param array $stubMethods
   *   Stub Methods.
   *
   * @return array
   *   Array with Stub enabled methods.
   */
  private function getStubEnabledMethods($stubMethods) {
    $methods = [];

    foreach ($stubMethods as $key => $enabled) {
      if ($enabled) {
        $methods[] = $key;
      }
    }

    return $methods;
  }

}
