<?php

namespace Drupal\stubby;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Base class for Parameter plugins.
 */
abstract class ParameterBase extends PluginBase implements ParameterInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The parameter ID.
   *
   * @var string
   */
  protected $uuid;

  /**
   * The weight of the parameter.
   *
   * @var int|string
   */
  protected $weight = '';

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getUuid() {
    return $this->uuid;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->weight = $weight;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function getKey() {
    return $this->configuration['key'];
  }

  /**
   * {@inheritdoc}
   */
  public function getResponseCode() {
    return $this->configuration['response_code'];
  }

  /**
   * {@inheritdoc}
   */
  public function getResponseMessage() {
    return $this->configuration['response_message'];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return [
      'uuid' => $this->getUuid(),
      'id' => $this->getPluginId(),
      'weight' => $this->getWeight(),
      'settings' => $this->configuration,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $configuration += [
      'settings' => [],
      'uuid' => '',
      'weight' => '',
    ];
    $this->configuration = $configuration['settings'] + $this->defaultConfiguration();
    $this->uuid = $configuration['uuid'];
    $this->weight = $configuration['weight'];
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'key' => '',
      'response_code' => 400,
      'response_message' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key'),
      '#default_value' => $this->configuration['key'],
      '#required' => TRUE,
    ];

    $form['response_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Response Code'),
      '#default_value' => $this->configuration['response_code'],
      '#required' => TRUE,
      '#description' => $this->t('The code to return if the above key is not found.'),
    ];

    $form['response_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Response Message'),
      '#default_value' => $this->configuration['response_message'],
      '#required' => TRUE,
      '#description' => $this->t('The message to return if the above key is not found.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Validate Response code.
    $response_code = $form_state->getValue('response_code');
    if ($response_code < 100 || $response_code >= 600) {
      $form_state->setErrorByName('response_code', 'Response code must be an integer value between 1xx and 5xx.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['key'] = $form_state->getValue('key');
    $this->configuration['response_code'] = $form_state->getValue('response_code');
    $this->configuration['response_message'] = $form_state->getValue('response_message');
  }

  /**
   * {@inheritdoc}
   */
  public function process() {
    // Key must exist or request is invalid.
    $current_request = $this->requestStack->getCurrentRequest();
    return $current_request->get($this->getKey(), FALSE);
  }

}
