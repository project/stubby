<?php

namespace Drupal\stubby;

use Drupal\Core\Plugin\DefaultLazyPluginCollection;

/**
 * A collection of parameters.
 */
class ParameterPluginCollection extends DefaultLazyPluginCollection {

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\stubby\ParameterInterface
   *   The parameter.
   */
  public function &get($instance_id) {
    // Reference passing.
    $instance = parent::get($instance_id);
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function sortHelper($aID, $bID) {
    $a_weight = $this->get($aID)->getWeight();
    $b_weight = $this->get($bID)->getWeight();
    if ($a_weight == $b_weight) {
      return 0;
    }

    return ($a_weight < $b_weight) ? -1 : 1;
  }

}
