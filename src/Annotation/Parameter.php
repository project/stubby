<?php

namespace Drupal\stubby\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Parameter item annotation object.
 *
 * @see \Drupal\stubby\ParameterManager
 * @see plugin_api
 *
 * @Annotation
 */
class Parameter extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
