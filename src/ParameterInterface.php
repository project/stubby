<?php

namespace Drupal\stubby;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Defines an interface for Parameter plugins.
 */
interface ParameterInterface extends PluginInspectionInterface, ConfigurableInterface, PluginFormInterface {

  /**
   * Returns the parameter label.
   *
   * @return string
   *   The parameter label.
   */
  public function label();

  /**
   * Returns the unique ID representing the parameter.
   *
   * @return string
   *   The parameter ID.
   */
  public function getUuid();

  /**
   * Returns the weight of the parameter.
   *
   * @return int|string
   *   Either the integer weight of the parameter, or an empty string.
   */
  public function getWeight();

  /**
   * Sets the weight for this parameter.
   *
   * @param int $weight
   *   The weight for this parameter.
   *
   * @return $this
   */
  public function setWeight($weight);

  /**
   * Key getter.
   */
  public function getKey();

  /**
   * Response code getter.
   */
  public function getResponseCode();

  /**
   * Response message getter.
   */
  public function getResponseMessage();

  /**
   * Processes a parameter plugin for pass or fail.
   *
   * @return mixed
   *   False on failure.
   */
  public function process();

}
