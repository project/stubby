<?php

namespace Drupal\stubby\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provides a controller to output stub responses.
 *
 * @package Drupal\stubby\Controller
 */
class Output extends ControllerBase {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The stub.
   *
   * @var \Drupal\stubby\Entity\StubInterface
   */
  protected $stub;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('file_system')
    );
  }

  /**
   * Constructs a new Output..
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system) {
    $this->entityManager = $entity_type_manager;
    $this->fileSystem = $file_system;
  }

  /**
   * Processes a stub into a response.
   *
   * @param string $stub
   *   The stub key.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JsonResponse object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function response($stub = NULL) {
    $data = [];

    /** @var \Drupal\stubby\Entity\StubInterface $stub */
    if (!$this->stub = $this->entityManager->getStorage('stub')->load($stub)) {
      // @todo Provide some feedback if/when this fails.
      return new JsonResponse($data);
    }

    // Allows Parameters to interject.
    $paramater_response = $this->processParameters();

    if (!empty($paramater_response)) {
      $bad_response = new JsonResponse([]);
      $bad_response->setStatusCode($paramater_response['code'], $paramater_response['message']);
      return $bad_response;
    }

    // Load the stub response.
    $json = $this->stub->getResponse();
    $data = Json::decode($json);

    $response = new JsonResponse($data);
    $response->setStatusCode($this->stub->getResponseCode(), $this->stub->getResponseMessage());

    return $response;

  }

  /**
   * Processes parameter plugins.
   *
   * @return array
   *   The response.
   */
  protected function processParameters() {
    /** @var \Drupal\stubby\ParameterInterface $parameter */
    foreach ($this->stub->getParameters() as $parameter) {
      if ($parameter->process() === FALSE) {
        // @todo Add some response logging here.
        return [
          'code' => $parameter->getResponseCode(),
          'message' => $parameter->getResponseMessage(),
        ];
      }
    }

    return [];
  }

}
