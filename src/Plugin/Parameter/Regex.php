<?php

namespace Drupal\stubby\Plugin\Parameter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\stubby\ParameterBase;

/**
 * Provides a required parameter that also validates against a pattern.
 *
 * @Parameter(
 *   id = "regex",
 *   label = @Translation("Regex Parameter"),
 * )
 */
class Regex extends ParameterBase {

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return ['pattern' => ''] + parent::defaultConfiguration();
  }

  /**
   * Getter for the pattern.
   */
  public function getPattern() {
    return $this->configuration['pattern'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['pattern'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Regular expression'),
      '#description' => $this->t('Regex pattern to use for validating the parameter.'),
      '#default_value' => $this->getPattern(),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {

    // Ensure that the pattern is valid by testing it against a NULL value.
    $pattern = $form_state->getValue('pattern');
    if (!empty($pattern)) {
      $valid = @preg_match($pattern, NULL);
      if ($valid === FALSE) {
        $form_state->setErrorByName('pattern', $this->t('Invalid regular expression pattern.'));
      }
    }

    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['pattern'] = $form_state->getValue('pattern');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function process() {
    // Key is required and must match.
    $current_request = $this->requestStack->getCurrentRequest();
    $value = $current_request->get($this->getKey(), FALSE);
    return $value ? (bool) preg_match($this->configuration['pattern'], $value) : FALSE;
  }

}
