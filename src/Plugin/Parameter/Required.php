<?php

namespace Drupal\stubby\Plugin\Parameter;

use Drupal\stubby\ParameterBase;

/**
 * Provides a required parameter..
 *
 * @Parameter(
 *   id = "required",
 *   label = @Translation("Required Parameter"),
 * )
 */
class Required extends ParameterBase {
}
