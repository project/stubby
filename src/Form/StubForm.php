<?php

namespace Drupal\stubby\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\stubby\ParameterManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for managing Stubs.
 */
class StubForm extends EntityForm {

  /**
   * The parameter manager.
   *
   * @var \Drupal\stubby\ParameterManager
   */
  protected $parameterManager;

  /**
   * The Stubby Settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.parameter'),
      $container->get('config.factory')
    );
  }

  /**
   * StubForm constructor.
   *
   * @param \Drupal\stubby\ParameterManager $parameter_manager
   *   The parameter manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ParameterManager $parameter_manager, ConfigFactoryInterface $config_factory) {
    $this->parameterManager = $parameter_manager;
    $this->config = $config_factory->get('stubby.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $user_input = $form_state->getUserInput();
    $form['#tree'] = TRUE;

    $stub = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $stub->label(),
      '#description' => $this->t("Label for the Stub."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $stub->id(),
      '#machine_name' => [
        'exists' => '\Drupal\stubby\Entity\Stub::load',
      ],
      '#disabled' => !$stub->isNew(),
    ];

    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path'),
      '#default_value' => $stub->getPath(),
      '#required' => TRUE,
    ];

    $form['status'] = [
      '#title' => $this->t('Enabled'),
      '#type' => 'checkbox',
      '#default_value' => $stub->status(),
      '#description' => $this->t('Turn the stub on or off.'),
    ];

    $form['methods'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed Methods'),
      '#options' => [
        'GET' => $this->t('GET'),
        'POST' => $this->t('POST'),
        'PUT' => $this->t('PUT'),
        'DELETE' => $this->t('DELETE'),
        'HEAD' => $this->t('HEAD'),
      ],
      '#desctiption' => $this->t('Select available methods for this path, leave blank to accept all methods.'),
      '#default_value' => $stub->getMethods(),
    ];

    $form['response_code'] = [
      '#title' => $this->t('Response Code'),
      '#type' => 'textfield',
      '#description' => $this->t('The HTTP response code to return.'),
      '#max' => 3,
      '#required' => TRUE,
      '#default_value' => $stub->getResponseCode(),
    ];

    $form['response_message'] = [
      '#title' => $this->t('Response Message'),
      '#type' => 'textfield',
      '#description' => $this->t('The HTTP response message, useful to test error response codes.'),
      '#default_value' => $stub->getResponseMessage(),
    ];

    $json_file = $stub->loadJsonFile();
    $upload_location = $this->config->get('file_location') . '://stubby/';
    $form['response_file'] = [
      '#title' => $this->t('JSON File Response'),
      '#type' => 'managed_file',
      '#upload_location' => $upload_location,
      '#upload_validators' => [
        'file_validate_extensions' => ['txt, json'],
      ],
      '#default_value' => $json_file ? ['0' => $json_file->id()] : NULL,
    ];

    $form['response_paste'] = [
      '#title' => $this->t('JSON Pasted Response'),
      '#type' => 'textarea',
      '#description' => $this->t('Alternatively paste a response here. This will override any previously entered file.'),
    ];

    // Only show parameters once a stub exists.
    if (!$stub->isNew()) {
      // Build the list of existing parameters for this stub.
      $form['parameters'] = [
        '#type' => 'table',
        '#header' => [
          $this->t('Parameter'),
          $this->t('Key'),
          $this->t('Response'),
          $this->t('Weight'),
          $this->t('Operations'),
        ],
        '#tabledrag' => [
          [
            'action' => 'order',
            'relationship' => 'sibling',
            'group' => 'parameter-order-weight',
          ],
        ],
        '#attributes' => [
          'id' => 'stub-parameters',
        ],
        '#empty' => $this->t('There are currently no parameters in this stub. Add one by selecting an option below.'),
        // Render parameters below parent elements.
        '#weight' => 5,
      ];

      /** @var \Drupal\stubby\ParameterInterface $parameter */
      foreach ($this->entity->getParameters() as $parameter) {
        $key = $parameter->getUuid();
        $form['parameters'][$key]['#attributes']['class'][] = 'draggable';
        $form['parameters'][$key]['#weight'] = isset($user_input['parameters']) ? $user_input['parameters'][$key]['weight'] : NULL;
        $form['parameters'][$key]['parameter'] = [
          '#tree' => FALSE,
          'data' => [
            'label' => [
              '#plain_text' => $parameter->label(),
            ],
          ],
        ];

        $form['parameters'][$key]['key'] = [
          'data' => [
            'label' => [
              '#plain_text' => $parameter->getKey(),
            ],
          ],
        ];

        $form['parameters'][$key]['code'] = [
          'data' => [
            'label' => [
              '#plain_text' => $parameter->getResponseCode(),
            ],
          ],
        ];

        $form['parameters'][$key]['weight'] = [
          '#type' => 'weight',
          '#title' => $this->t('Weight for @title', ['@title' => $parameter->label()]),
          '#title_display' => 'invisible',
          '#default_value' => $parameter->getWeight(),
          '#attributes' => [
            'class' => ['parameter-order-weight'],
          ],
        ];

        $links = [];
        $links['edit'] = [
          'title' => $this->t('Edit'),
          'url' => Url::fromRoute('stubby.parameter_edit_form', [
            'stub' => $this->entity->id(),
            'parameter' => $key,
          ]),
        ];
        $links['delete'] = [
          'title' => $this->t('Delete'),
          'url' => Url::fromRoute('stubby.parameter_delete_form', [
            'stub' => $this->entity->id(),
            'parameter' => $key,
          ]),
        ];
        $form['parameters'][$key]['operations'] = [
          '#type' => 'operations',
          '#links' => $links,
        ];
      }

      // Build the new parameter addition form and add it to the parameter list.
      $new_parameter_options = [];
      $parameters = $this->parameterManager->getDefinitions();
      uasort($parameters, function ($a, $b) {
        return Unicode::strcasecmp($a['label'], $b['label']);
      });
      foreach ($parameters as $parameter => $definition) {
        $new_parameter_options[$parameter] = $definition['label'];
      }
      $form['parameters']['new'] = [
        '#tree' => FALSE,
        '#weight' => isset($user_input['weight']) ? $user_input['weight'] : NULL,
        '#attributes' => ['class' => ['draggable']],
      ];
      $form['parameters']['new']['parameter'] = [
        'data' => [
          'new' => [
            '#type' => 'select',
            '#title' => $this->t('Parameter'),
            '#title_display' => 'invisible',
            '#options' => $new_parameter_options,
            '#empty_option' => $this->t('Select a parameter type'),
          ],
          [
            'add' => [
              '#type' => 'submit',
              '#value' => $this->t('Add'),
              '#validate' => ['::parameterValidate'],
              '#submit' => ['::submitForm', '::parameterSave'],
            ],
          ],
        ],
        '#prefix' => '<div class="parameter-new">',
        '#suffix' => '</div>',
      ];

      $form['parameters']['new']['key'] = [
        'data' => [
          'label' => [
            '#plain_text' => '',
          ],
        ],
      ];

      $form['parameters']['new']['code'] = [
        'data' => [
          'label' => [
            '#plain_text' => '',
          ],
        ],
      ];

      $form['parameters']['new']['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for new parameter'),
        '#title_display' => 'invisible',
        '#default_value' => count($this->entity->getParameters()) + 1,
        '#attributes' => ['class' => ['parameter-order-weight']],
      ];
      $form['parameters']['new']['operations'] = [
        'data' => [],
      ];
    }

    return $form;
  }

  /**
   * Validate handler for parameters.
   */
  public function parameterValidate($form, FormStateInterface $form_state) {
    if (!$form_state->getValue('new')) {
      $form_state->setErrorByName('new', $this->t('Select a parameter type to add.'));
    }
  }

  /**
   * Submit handler for parameters.
   */
  public function parameterSave($form, FormStateInterface $form_state) {
    $this->save($form, $form_state);
    $form_state->setRedirect(
      'stubby.parameter_add_form',
      [
        'stub' => $this->entity->id(),
        'parameter' => $form_state->getValue('new'),
      ],
      ['query' => ['weight' => $form_state->getValue('weight')]]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Validate path.
    $path = $form_state->getValue('path');

    if (strpos($path, '/') !== 0) {
      $form_state->setErrorByName('path', 'The alias path has to start with a slash.');
    }

    // @todo See if there's a better way to do this.
    if (filter_var('http://www.example.com' . $path, FILTER_VALIDATE_URL) === FALSE) {
      $form_state->setErrorByName('path', 'The alias path needs to be a valid URL.');
    }

    // Validate Response code.
    $response_code = $form_state->getValue('response_code');
    if ($response_code < 100 || $response_code >= 600) {
      $form_state->setErrorByName('response_code', 'Response code must be an integer value between 1xx and 5xx.');
    }

    // Convert JSON to file.
    $pasted_json = $form_state->getValue('response_paste');
    if (!empty($pasted_json)) {
      $json = Json::decode($pasted_json);

      // Validate JSON first.
      if ($json !== NULL) {
        $json = Json::encode($json);
        $stub = $this->entity;
        if ($file = $stub->createJsonFile($json)) {
          $form_state->setValue('response_file', ['0' => $file->id()]);
        }
        else {
          $form_state->setErrorByName('response_paste', $this->t('Could not save JSON to the specified location, check your file location in settings.'));
        }
      }
      else {
        $form_state->setErrorByName('response_paste', $this->t('The pasted value must be valid JSON.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Update parameter weights.
    if (!$form_state->isValueEmpty('parameters')) {
      $this->updateParameterWeights($form_state->getValue('parameters'));
    }
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\stubby\Entity\StubInterface $stub */
    $stub = $this->entity;

    $json_fid = $form_state->getValue(['response_file', 0]);
    $old_file = $stub->loadJsonFile();
    // We have a file, either old or new.
    if ($json_fid) {
      /** @var \Drupal\file\FileInterface $file */
      $file = $this->entityTypeManager->getStorage('file')->load($json_fid);

      // New file or overwrite.
      if (!$old_file || $old_file->id() !== $file->id()) {
        $file->setPermanent();
        $file->save();

        // Remove the old file.
        if ($old_file) {
          $old_file->delete();
        }

        // Save new data to config.
        $stub->set('response_data', [
          'uri' => $file->getFileUri(),
          'data' => $stub->getResponse($file),
        ]);
      }
    }
    elseif ($old_file) {
      $old_file->delete();
      $stub->set('response_data', NULL);
    }

    if ($form_state->isValueEmpty('parameters')) {
      $stub->set('parameters', []);
    }

    // Save the stub.
    $status = $stub->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Stub.', [
          '%label' => $stub->label(),
        ]));
        $form_state->setRedirectUrl($this->entity->toUrl('edit-form'));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Stub.', [
          '%label' => $stub->label(),
        ]));
        $form_state->setRedirectUrl($stub->toUrl('collection'));
    }
  }

  /**
   * Updates parameter weights.
   *
   * @param array $parameters
   *   Associative array with parameters having parameter uuid as keys and array
   *   with parameter settings as values.
   */
  protected function updateParameterWeights(array $parameters) {
    foreach ($parameters as $uuid => $parameter_settings) {
      if ($this->entity->getParameters()->has($uuid)) {
        $this->entity->getParameter($uuid)->setWeight($parameter_settings['weight']);
      }
    }
  }

}
