<?php

namespace Drupal\stubby\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\stubby\Entity\StubInterface;

/**
 * Provides a form to edit parameter plugins.
 */
class ParameterEditForm extends ParameterFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, StubInterface $stub = NULL, $parameter = NULL) {
    $form = parent::buildForm($form, $form_state, $stub, $parameter);
    $form['#title'] = $this->t('Edit %label parameter', ['%label' => $this->parameter->label()]);
    $form['actions']['submit']['#value'] = $this->t('Update parameter');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareParameter($parameter) {
    return $this->stub->getParameter($parameter);
  }

}
