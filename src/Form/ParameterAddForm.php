<?php

namespace Drupal\stubby\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\stubby\Entity\StubInterface;

/**
 * Provides a form to edit parameter plugins.
 */
class ParameterAddForm extends ParameterFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, StubInterface $stub = NULL, $parameter = NULL) {
    $form = parent::buildForm($form, $form_state, $stub, $parameter);
    $form['#title'] = $this->t('Add %label parameter', ['%label' => $this->parameter->label()]);
    $form['actions']['submit']['#value'] = $this->t('Add parameter');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareParameter($parameter) {
    $parameter = $this->parameterManager->createInstance($parameter);
    // Set the initial weight so this parameter comes last.
    $parameter->setWeight(count($this->stub->getParameters()));
    return $parameter;
  }

}
