<?php

namespace Drupal\stubby\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\stubby\Entity\StubInterface;

/**
 * Form for deleting a parameter.
 *
 * @internal
 */
class ParameterDeleteForm extends ConfirmFormBase {

  /**
   * The stub.
   *
   * @var \Drupal\stubby\Entity\StubInterface
   */
  protected $stub;

  /**
   * The parameter.
   *
   * @var \Drupal\stubby\ParameterInterface
   */
  protected $parameter;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the @parameter parameter from the %stub stub?', [
      '%stub' => $this->stub->label(),
      '@parameter' => $this->parameter->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->stub->toUrl('edit-form');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'parameter_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, StubInterface $stub = NULL, $parameter = NULL) {
    $this->stub = $stub;
    $this->parameter = $this->stub->getParameter($parameter);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->stub->deleteParameter($this->parameter);
    $this->messenger()->addStatus($this->t('The parameter %name has been deleted.', ['%name' => $this->parameter->label()]));
    $form_state->setRedirectUrl($this->stub->toUrl('edit-form'));
  }

}
