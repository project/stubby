<?php

namespace Drupal\stubby\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings for Stubby.
 *
 * @package Drupal\stubby\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The streamwrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('stream_wrapper_manager'),
      $container->get('file_system')
    );
  }

  /**
   * SettingsForm construct.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config factory.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StreamWrapperManagerInterface $stream_wrapper_manager, FileSystemInterface $file_system) {
    parent::__construct($config_factory);
    $this->streamWrapperManager = $stream_wrapper_manager;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return ['stubby.settings'];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'stubby_settings_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('stubby.settings');

    // Any visible, writable wrapper can potentially be used for the files
    // directory, including a remote file system that integrates with a CDN.
    $options = $this->streamWrapperManager->getDescriptions(StreamWrapperInterface::WRITE_VISIBLE);

    $form['file_location'] = [
      '#type' => 'radios',
      '#title' => $this->t('File location to place JSON Stubs'),
      '#default_value' => $config->get('file_location'),
      '#options' => $options,
      '#required' => TRUE,
      '#description' => $this->t('The use of public files is more efficient, but does not provide any access control.'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Ensure we can write here.
    $file_location = $form_state->getValue('file_location') . '://stubby/';
    if (!$this->fileSystem->prepareDirectory($file_location, FileSystemInterface::CREATE_DIRECTORY)) {
      $form_state->setErrorByName('file_location', $this->t('The specifid directory is not writable.'));
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('stubby.settings')->set('file_location', $form_state->getValue('file_location'))->save();
  }

}
