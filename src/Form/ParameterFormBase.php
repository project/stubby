<?php

namespace Drupal\stubby\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\stubby\Entity\StubInterface;
use Drupal\stubby\ParameterManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a form base for parameter plugins.
 */
abstract class ParameterFormBase extends FormBase {

  /**
   * The parameter manager.
   *
   * @var \Drupal\stubby\ParameterManager
   */
  protected $parameterManager;

  /**
   * The stub.
   *
   * @var \Drupal\stubby\Entity\StubInterface
   */
  protected $stub;

  /**
   * The parameter.
   *
   * @var \Drupal\stubby\ParameterInterface
   */
  protected $parameter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.parameter')
    );
  }

  /**
   * StubForm constructor.
   *
   * @param \Drupal\stubby\ParameterManager $parameter_manager
   *   The parameter manager.
   */
  public function __construct(ParameterManager $parameter_manager) {
    $this->parameterManager = $parameter_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'parameter_form';
  }

  /**
   * Builds a param form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param \Drupal\stubby\Entity\StubInterface $stub
   *   The stub..
   * @param string $parameter
   *   The parameter ID.
   *
   * @return array
   *   The form structure.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function buildForm(array $form, FormStateInterface $form_state, StubInterface $stub = NULL, $parameter = NULL) {
    $this->stub = $stub;
    try {
      $this->parameter = $this->prepareParameter($parameter);
    }
    catch (PluginNotFoundException $e) {
      throw new NotFoundHttpException("Invalid parameter id: '$parameter'.");
    }
    $request = $this->getRequest();

    $form['uuid'] = [
      '#type' => 'value',
      '#value' => $this->parameter->getUuid(),
    ];
    $form['id'] = [
      '#type' => 'value',
      '#value' => $this->parameter->getPluginId(),
    ];

    $form['settings'] = [];
    $subform_state = SubformState::createForSubform($form['settings'], $form, $form_state);
    $form['settings'] = $this->parameter->buildConfigurationForm($form['settings'], $subform_state);
    $form['settings']['#tree'] = TRUE;

    // Check the URL for a weight, then the parameter, otherwise use default.
    $form['weight'] = [
      '#type' => 'hidden',
      '#value' => $request->query->has('weight') ? (int) $request->query->get('weight') : $this->parameter->getWeight(),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
    ];
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => $this->stub->toUrl('edit-form'),
      '#attributes' => ['class' => ['button']],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // The parameter configuration is stored in the 'data' key in the form,
    // pass that through for validation.
    $this->parameter->validateConfigurationForm($form['settings'], SubformState::createForSubform($form['settings'], $form, $form_state));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();

    // The parameter configuration is stored in the 'data' key in the form,
    // pass that through for submission.
    $this->parameter->submitConfigurationForm($form['settings'], SubformState::createForSubform($form['settings'], $form, $form_state));

    $this->parameter->setWeight($form_state->getValue('weight'));
    if (!$this->parameter->getUuid()) {
      $this->stub->addParameter($this->parameter->getConfiguration());
    }
    $this->stub->save();

    $this->messenger()->addStatus($this->t('The parameter was successfully added.'));
    $form_state->setRedirectUrl($this->stub->toUrl('edit-form'));
  }

  /**
   * Converts an parameter ID into an object.
   *
   * @param string $parameter
   *   The parameter ID.
   *
   * @return \Drupal\stubby\ParameterInterface
   *   The parameter object.
   */
  abstract protected function prepareParameter($parameter);

}
