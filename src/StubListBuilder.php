<?php

namespace Drupal\stubby;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Stub entities.
 */
class StubListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'label' => $this->t('Stub'),
      'id' => $this->t('Machine Name'),
      'path' => $this->t('Path'),
      'methods' => $this->t('Methods'),
      'response_code' => $this->t('Response'),
      'status' => $this->t('Status'),
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $methods = array_filter($entity->getMethods());
    $row = [
      'label' => $entity->label(),
      'id' => $entity->id(),
      'path' => $entity->getPath(),
      'methods' => empty($methods) ? $this->t('Any') : implode(",", $methods),
      'response_code' => $entity->getResponseCode(),
      'status' => $entity->status() ? $this->t('Enabled') : $this->t('Disabled'),
    ];
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    // Remove destination URL from the edit link.
    if (isset($operations['edit'])) {
      $operations['edit']['url'] = $entity->toUrl('edit-form');
    }
    return $operations;
  }

}
