<?php

namespace Drupal\stubby\Entity;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\FileInterface;
use Drupal\stubby\ParameterInterface;
use Drupal\stubby\ParameterPluginCollection;

/**
 * Defines the Stub entity.
 *
 * @ConfigEntityType(
 *   id = "stub",
 *   label = @Translation("Stub"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\stubby\StubListBuilder",
 *     "form" = {
 *       "add" = "Drupal\stubby\Form\StubForm",
 *       "edit" = "Drupal\stubby\Form\StubForm",
 *       "delete" = "Drupal\stubby\Form\StubDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\stubby\StubHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "stub",
 *   admin_permission = "administer stubs",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "path",
 *     "status",
 *     "methods",
 *     "response_code",
 *     "response_message",
 *     "response_data",
 *     "parameters",
 *   },
 *   links = {
 *     "canonical" = "/admin/config/development/stub/{stub}",
 *     "add-form" = "/admin/config/development/stub/add",
 *     "edit-form" = "/admin/config/development/stub/{stub}/edit",
 *     "delete-form" = "/admin/config/development/stub/{stub}/delete",
 *     "collection" = "/admin/config/development/stub"
 *   }
 * )
 */
class Stub extends ConfigEntityBase implements StubInterface, EntityWithPluginCollectionInterface {

  /**
   * The Stub ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Stub label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Stub path.
   *
   * @var string
   */
  protected $path = '';

  /**
   * The Stub methods.
   *
   * @var array
   */
  protected $methods = [];

  /**
   * The Stub response code.
   *
   * @var string
   */
  protected $response_code = 200;

  /**
   * The Stub response message.
   *
   * @var string
   */
  protected $response_message = 'OK';

  /**
   * The Stub response data.
   *
   * @var string
   */
  protected $response_data;

  /**
   * The stub parameters.
   *
   * @var array
   */
  protected $parameters = [];

  /**
   * Holds the collection of parameters that are used by this stub.
   *
   * @var \Drupal\stubby\ParameterPluginCollection
   */
  protected $parameterCollection;

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // Rebuild routes if it's changed.
    if ($update && !empty($this->original)) {
      if ($this->getPath() !== $this->original->getPath() ||
        $this->getMethods() !== $this->original->getMethods() ||
        $this->status() !== $this->original->status()
      ) {
        \Drupal::service('router.builder')->rebuild();
      }
    }
    else {
      // Rebuild new ones if they are 'Enabled'.
      if ($this->status()) {
        \Drupal::service('router.builder')->rebuild();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPath() {
    return $this->path;
  }

  /**
   * {@inheritdoc}
   */
  public function getMethods() {
    return $this->methods;
  }

  /**
   * {@inheritdoc}
   */
  public function getResponseCode() {
    return $this->response_code;
  }

  /**
   * {@inheritdoc}
   */
  public function getResponseMessage() {
    return $this->response_message;
  }

  /**
   * {@inheritdoc}
   */
  public function getResponseData() {
    return $this->response_data;
  }

  /**
   * {@inheritdoc}
   */
  public function getParameter($parameter) {
    return $this->getParameters()->get($parameter);
  }

  /**
   * {@inheritdoc}
   */
  public function getParameters() {
    if (!$this->parameterCollection) {
      $this->parameterCollection = new ParameterPluginCollection($this->getParameterPluginManager(), $this->parameters);
      $this->parameterCollection->sort();
    }
    return $this->parameterCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return ['parameters' => $this->getParameters()];
  }

  /**
   * {@inheritdoc}
   */
  public function addParameter(array $configuration) {
    $configuration['uuid'] = $this->uuidGenerator()->generate();
    $this->getParameters()->addInstanceId($configuration['uuid'], $configuration);
    return $configuration['uuid'];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteParameter(ParameterInterface $parameter) {
    $this->getParameters()->removeInstanceId($parameter->getUuid());
    $this->save();
    return $this;
  }

  /**
   * Returns the parameter plugin manager.
   *
   * @return \Drupal\Component\Plugin\PluginManagerInterface
   *   The parameter plugin manager.
   */
  protected function getParameterPluginManager() {
    return \Drupal::service('plugin.manager.parameter');
  }

  /**
   * {@inheritdoc}
   */
  public function createJsonFile($json, $file_name = NULL) {
    $json_array = Json::decode($json);
    if ($json_array !== NULL) {
      $config = \Drupal::config('stubby.settings');
      $file_name = $file_name ?? $config->get('file_location') . '://stubby/' . $this->id() . '.json';
      $json = Json::encode($json_array);
      return \Drupal::service('file.repository')->writeData($json, $file_name, FileSystemInterface::EXISTS_REPLACE);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function loadJsonFile() {
    $response_data = $this->getResponseData();
    if (!empty($response_data['uri'])) {
      $uri = $response_data['uri'];

      $files = \Drupal::entityTypeManager()->getStorage('file')->loadByProperties(['uri' => $uri]);
      if (count($files)) {
        foreach ($files as $item) {
          $real_path = \Drupal::service('file_system')->realpath($uri);
          if ($item->getFileUri() === $uri && file_exists($real_path)) {
            $file = $item;
            break;
          }
        }
      }

      // File doesn't exists but we have the data.
      if (!isset($file) && !empty($response_data['data']) && !UrlHelper::isExternal($uri)) {
        $file = $this->createJsonFile($response_data['data'], $response_data['uri']);
      }
    }

    return $file ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getResponse(FileInterface $file = NULL) {
    $data = '';
    $file = $file ?? $this->loadJsonFile();
    if ($file) {
      $real_path = \Drupal::service('file_system')->realpath($file->getFileUri());
      if (file_exists($real_path)) {
        $data = file_get_contents($real_path) ?? '';
      }
    }
    return $data;
  }

}
