<?php

namespace Drupal\stubby\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\file\FileInterface;
use Drupal\stubby\ParameterInterface;

/**
 * Provides an interface for defining Stub entities.
 */
interface StubInterface extends ConfigEntityInterface {

  /**
   * Path Getter.
   */
  public function getPath();

  /**
   * Method Getter.
   */
  public function getMethods();

  /**
   * Response code Getter.
   */
  public function getResponseCode();

  /**
   * Response Message Getter.
   */
  public function getResponseMessage();

  /**
   * Response data Getter.
   */
  public function getResponseData();

  /**
   * Parameter Getter.
   *
   * @param string $parameter
   *   The parameter uuid.
   *
   * @return \Drupal\stubby\ParameterInterface
   *   A loaded parameter.
   */
  public function getParameter($parameter);

  /**
   * Load sorted parameters.
   */
  public function getParameters();

  /**
   * Adds a parameter to a stub collection.
   */
  public function addParameter(array $configuration);

  /**
   * Removes a parameter from a stub collection.
   */
  public function deleteParameter(ParameterInterface $parameter);

  /**
   * Creates a managed file for a stub.
   *
   * @param string $json
   *   The JSON to save.
   * @param string $file_name
   *   An optional name to save the file as.
   *
   * @return \Drupal\file\FileInterface|false
   *   The saved file or false.
   */
  public function createJsonFile($json, $file_name = NULL);

  /**
   * Loads the file and creates one if missing.
   *
   * @return \Drupal\file\FileInterface|false
   *   The loaded file or false on failuire.
   */
  public function loadJsonFile();

  /**
   * Outputs the contents of a the Stub.
   *
   * @param \Drupal\file\FileInterface|null $file
   *   A file to parse, otherwise use from stub.
   *
   * @return string
   *   The JSON response.
   */
  public function getResponse(FileInterface $file = NULL);

}
